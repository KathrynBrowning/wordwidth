/**
 * 
 */
package edu.westga.wordwidth.model;

/**
 * This class calculates the width of a word using certain criteria.
 * 
 * @author Kathryn Browning
 * @version September 18, 2015
 *
 */
public class StringUtils {

	/**
	 * Finds the "width" of a word according to the following criteria:
	 * <ul>
	 * <li>'i' and 'j' are worth 1
	 * <li>'m' is worth 3
	 * <li>all other lowercase letters are worth 2
	 * <li>'I' is worth 2
	 * <li>'M' is worth 4
	 * <li>all other uppercase letters are worth 3
	 * </ul>
	 * <p>
	 * if the word contains any non-alphabetic character an
	 * IllegalArgumentException is thrown
	 * </p>
	 * 
	 * @param word
	 *            the word that will have its width calculated
	 * @return returns the sum of the letters' widths
	 */
	public static int findWordWidth(String word) {
		if (word == null) {
			throw new IllegalArgumentException("word should not be null");
		}
		if (word.isEmpty()) {
			return 0;
		}
		if(!word.matches("[a-zA-Z]+")) {
			throw new IllegalArgumentException("word can only contain letters of the alphabet");
		}				

		int sum = 0;
		for (char letter : word.toCharArray()) {
			if (Character.isLowerCase(letter)) {
				sum = lowercase(sum, letter);
			}
			if (Character.isUpperCase(letter)) {
				sum = uppercase(sum, letter);
			}
		}
		return sum;

	}

	private static int lowercase(int sum, char letter) {
		if (letter == 'i' || letter == 'j') {
			sum += 1;
		} else if (letter == 'm') {
			sum += 3;
		} else {
			sum += 2;
		}

		return sum;
	}

	private static int uppercase(int sum, char letter) {
		if (letter == 'I') {
			sum += 2;
		} else if (letter == 'M') {
			sum += 4;
		} else {
			sum += 3;
		}

		return sum;
	}
}
