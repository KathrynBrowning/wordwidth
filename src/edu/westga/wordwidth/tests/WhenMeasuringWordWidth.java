/**
 * 
 */
package edu.westga.wordwidth.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.wordwidth.model.StringUtils;

/**
 * Tests the StringUtils methods.
 * 
 * @author Kathryn Browning
 * @version September 20, 2015
 *
 */
public class WhenMeasuringWordWidth {

	/**
	 * A null word should throw an exception.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void nullWordShouldThrowException() {
		StringUtils.findWordWidth(null);
	}

	/**
	 * A word that contains non-alphabetic characters should throw an exception.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void wordWithNonAlphabeticCharacterShouldThrowException() {
		StringUtils.findWordWidth("4");
		StringUtils.findWordWidth("!");
		StringUtils.findWordWidth("a$55p");
	}
	
	/**
	 * A word that is an empty string should return a width of 0.
	 */
	@Test
	public void wordIsEmptyString() {
		assertEquals(StringUtils.findWordWidth(""), 0);
	}

	/**
	 * The word jimijam should have a width of 12.
	 */
	@Test
	public void jimijamShouldHaveWidthOf12() {
		int width = StringUtils.findWordWidth("jimijam");
		assertEquals(12, width);
	}

	/**
	 * The word jimiJAM should have a width of 16.
	 */
	@Test
	public void jimiJAMShouldHaveWidthof16() {
		int width = StringUtils.findWordWidth("jimiJAM");
		assertEquals(16, width);
	}

	/**
	 * The word JIMIJAM should have a width of 21.
	 */
	@Test
	public void JIMIJAMShouldHaveWidthOf21() {
		int width = StringUtils.findWordWidth("JIMIJAM");
		assertEquals(21, width);
	}
}
